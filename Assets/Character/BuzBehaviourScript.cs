﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuzBehaviourScript : MonoBehaviour
{
    private float x, y;
    private SpriteRenderer spriteRenderer;
    // Start is called before the first frame update
    void Start()
    {
        spriteRenderer = this.GetComponent<SpriteRenderer>();
    }
 
    // Update is called once per frame
    void Update()
    {
        x = this.transform.position.x;
        y = this.transform.position.y;
 
        if (Input.GetAxis("Horizontal") > 0 ) 
        {
            this.transform.position = new Vector2(x + 0.1f, y);
            spriteRenderer.flipX = false;
        }
        else if (Input.GetAxis("Horizontal") < 0 ) 
        {
            this.transform.position = new Vector2(x - 0.1f, y);
            spriteRenderer.flipX = true;
        }

        if (Input.GetAxis("Vertical") > 0 ) 
        {
            this.transform.position = new Vector2(x, y + 0.1f);
            spriteRenderer.flipX = false;
        }
        else if (Input.GetAxis("Vertical") < 0 ) 
        {
            this.transform.position = new Vector2(x, y - 0.1f);
            spriteRenderer.flipX = true;
        }
    }

}
